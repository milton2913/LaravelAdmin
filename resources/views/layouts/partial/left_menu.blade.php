<ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>

    <li class="sidebar-search-wrapper">

        <form class="sidebar-search  sidebar-search-bordered" action="page_general_search_3.html" method="POST">
            <a href="javascript:;" class="remove">
                <i class="icon-close"></i>
            </a>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
            </div>
        </form>
        <!-- END RESPONSIVE QUICK SEARCH FORM -->
    </li>
    <li class="nav-item start ">
        <a href="{{url('admin')}}" class="nav-link nav-toggle">
            <i class="icon-home"></i>
            <span class="title">Dashboard</span>

        </a>

    </li>
    @if(Auth::user()->role->name ==="Admin")
    <li class="heading">
        <h3 class="uppercase">Features</h3>
    </li>
    <li class="nav-item  ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-user"></i>
            <span class="title">User Settings</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li><a href="{{ url('/' . Config("authorization.route-prefix") . '/users') }}" >Users</a></li>
            <li><a href="{{ url('/' . Config("authorization.route-prefix") . '/roles') }}" >Role</a></li>
            <li><a href="{{ url('/' . Config("authorization.route-prefix") . '/permissions') }}">Permission</a></li>
        </ul>
    </li>
    <li class="nav-item  ">
        <a href="javascript:;" class="nav-link nav-toggle">
            <i class="icon-puzzle"></i>
            <span class="title">General Settings</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub-menu">
            <li> <a href="{{ url('/' . Config("authorization.route-prefix") . '/Settings1') }}"  >Settings1</a>
            </li>
            <li><a href="{{ url('/' . Config("authorization.route-prefix") . '/Settings2') }}"  >Settings2</a></li>

        </ul>
    </li>

        @else
        <li><a href="{{ url('/' . Config("authorization.route-prefix") . '/profile') }}"  > <i class="fa fa-hand-o-right"></i>
                <span class="title">Profile</span></a></li>

@endif
</ul>
                