<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::group([
    'prefix' => Config("authorization.route-prefix"),
    'middleware' => ['web', 'auth']],
    function() {
        Route::group(['middleware' => Config("authorization.middleware")], function() {
            Route::resource('users', 'Admin\\UsersController', ['except' => [
                'create', 'store', 'show'
            ]]);
            Route::resource('roles', 'Admin\\RolesController');
            Route::get('/permissions', 'Admin\\PermissionsController@index');
            Route::post('/permissions', 'Admin\\PermissionsController@update');
            Route::post('/permissions/getSelectedRoutes', 'Admin\\PermissionsController@getSelectedRoutes');
        });

        Route::get('/', function () {
            return view('vendor.authorize.welcome');
        });
    });

Route::get('register/verify/{token}', 'Auth\RegisterController@verify');