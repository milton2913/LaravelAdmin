<?php

namespace App\Http\Controllers\Auth;
use DB;
use Mail;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Mail\EmailVerification;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session;




class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'email_token' => str_random(25),
        ]);
    }
    public function register(Request $request)
    {
        // Laravel validation
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
        // Using database transactions is useful here because stuff happening is actually a transaction
        // I don't know what I said in the last line! Weird!
        DB::beginTransaction();
        try {
            $user = $this->create($request->all());
            // After creating the user send an email with the random token generated in the create method above
            $email = new EmailVerification(new User(['email_token' => $user->email_token, 'name' => $user->name]));

            Mail::to($user->email)->send($email);
            DB::commit();
            $notification = array(
                'message' => 'Account activation code has been sent to your email, now activate your account!',
                'alert-type' => 'success'
            );
            Session::flash('notification',$notification);
            return redirect('login');
        } catch (Exception $e) {
            DB::rollback();
            return back();
        }
    }


    public function verify($token)
    {

        $user = User::where("verified",0)->where('email_token',$token)->get()->toArray();
        if(empty($user)){

            $notification = array(
                'message' => 'You token is invalid or already verified.',
                'alert-type' => 'success'
            );
            Session::flash('notification',$notification);
            return redirect('login');
        }else{
            User::where('email_token',$token)->firstOrFail()->verified();
            $notification = array(
                'message' => 'Your account is activated. Please log in now.',
                'alert-type' => 'success'
            );
            Session::flash('notification',$notification);
            return redirect('login');
        }
    }
}
