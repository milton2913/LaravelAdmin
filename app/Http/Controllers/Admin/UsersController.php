<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Role;
use Session;
class UsersController extends Controller
{

    public function index()
    {
        $users = User::paginate(15);
        return view('vendor.authorize.users.index', compact('users'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::pluck('name', 'id');

        return view('vendor.authorize.users.edit', compact('user', 'roles'));
    }

    public function update($id, Request $request)
    {

        $requestData = $request->all();
        $user = User::findOrFail($id);
        $user->update($requestData);

        $notification = array(
            'message' => 'User has been successfully updated!',
            'alert-type' => 'success'
        );
        Session::flash('notification',$notification);

        return redirect(Config("authorization.route-prefix") . '/users');
    }

    public function destroy($id)
    {
        User::destroy($id);

        Session::flash('flash_message', 'User deleted!');

        return redirect(Config("authorization.route-prefix") . '/users');
    }
}
