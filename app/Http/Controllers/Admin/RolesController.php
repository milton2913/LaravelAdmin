<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;


use App\Http\Requests;

use App\Role;
use Illuminate\Http\Request;
use Session;
use App\Http\Requests\RoleRequest;
class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::paginate(15);

        return view('vendor.authorize.roles.index', compact('roles'));
    }

    public function create()
    {
        return view('vendor.authorize.roles.create');
    }

    public function store(RoleRequest $request)
    {
        $requestData = $request->all();
        Role::create($requestData);
        $notification = array(
            'message' => 'Role Created is Successfully!',
            'alert-type' => 'success'
        );
        Session::flash('notification',$notification);
        return redirect(Config("authorization.route-prefix") . '/roles');
    }

    public function show($id)
    {
        $role = Role::findOrFail($id);

        return view('vendor.authorize.roles.show', compact('role'));
    }

    public function edit($id)
    {
        $role = Role::findOrFail($id);

        return view('vendor.authorize.roles.edit', compact('role'));
    }

    public function update($id, RoleRequest $request)
    {

        $requestData = $request->all();
        $role = Role::findOrFail($id);
        $role->update($requestData);
        $notification = array(
            'message' => 'Role Updated is Successfully!',
            'alert-type' => 'success'
        );
        Session::flash('notification',$notification);
        return redirect(Config("authorization.route-prefix") . '/roles');
    }


    public function destroy($id)
    {
        Role::destroy($id);
        $notification = array(
            'message' => 'Role Deleted is Successfully!',
            'alert-type' => 'success'
        );
        Session::flash('notification',$notification);
        return redirect(Config("authorization.route-prefix") . '/roles');
    }
}
